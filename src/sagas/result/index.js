import GetWeekly from './get-weekly.saga'

const ResultSagas = {
  GetWeekly,
}

export default ResultSagas

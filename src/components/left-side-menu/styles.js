import { StyleSheet } from 'react-native'
import { Themes } from '../../ui';

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#20AEF6',
    // width: '70%',
    height: '100%',
  },
  backgroundImage: {
    width: '100%',
    height: '100%',
  },
  navTextActive: {
    textDecorationLine: 'underline',
    fontSize: 20,
    color: 'white',
  },
  navText: {
    color: 'black',
    fontSize: 20,
  },
  headerLogoImage: {
    resizeMode: 'contain',
    width: 260 * Themes.Metrics.ratioScreen,
    position: 'absolute',
    top: -40 * Themes.Metrics.ratioScreen,
    left: -30 * Themes.Metrics.ratioScreen,
  },
  profileOverviewContainer: {
    width: '100%',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
  },
  profileName: {
    fontWeight: 'bold',
    marginTop: 10,
    fontSize: 20,
    color: 'white',
  }
})

export default styles

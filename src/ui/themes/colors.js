const colors = {
  // green: '#1dcc8c',
  blue: '#00ACF0',
  orange: '#FF622F',
  // violet: '#977eb7',
  // pink: '#f76c8b',
  // red: '#fa7a61',
  // darkRed: '#d02d47',
  // lightGreen: '#21caad',
  // lightBlue: '#66abf8',
  gray: '#989898',
  lightGray: '#CBCBCB',

  screenBackground: '#E7E8E8',
  // drawerOverlayColor: 'rgba(82,213,56,0.9)',
  // inputTextColor: '#585858',
  // inputBorderColor: '#dedede',
  // headingColor: '#21caad',
  // buttonBackgroundColor: '#1dcc8c',
  // buttonBackgroundUnderlayColor: '#1ec286',
  // normalTextColor: '#3a3a3a',

  // borderColor: '#dedede',
  // borderColorError: '#ea373a',

  // btnColor1: '#304097',
  // btnColor2: 'red',
  // navColor: '#c92033',
  // max4dColor: '#800593',
}

export default colors

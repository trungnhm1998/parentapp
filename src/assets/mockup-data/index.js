import childs from './childs'
import results from './results'

const MockupData = {
  childs,
  results,
}

export default MockupData

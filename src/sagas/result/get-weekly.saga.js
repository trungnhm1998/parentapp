import { takeLatest, call, put } from 'redux-saga/effects'
import { AppTypes } from '../../redux/app'
import MockupData from '../../assets/mockup-data'

const proccessData = (rawData) => {
  return new Promise(async (resolve, reject) => {
    try {
      console.log('proccessing Data', rawData)
      const numberOfQa = [
        {
          x: 'mon',
          y: 0,
        },
        {
          x: 'tue',
          y: 0,
        },
        {
          x: 'wed',
          y: 0,
        },
        {
          x: 'thu',
          y: 0,
        },
        {
          x: 'fri',
          y: 0,
        },
        {
          x: 'sat',
          y: 0,
        },
        {
          x: 'sun',
          y: 0,
        },
      ]
      const numberCorrect = JSON.parse(JSON.stringify(numberOfQa))

      rawData.forEach((day) => {
        const regex = /\d/g
        const { name } = day
        const index = name.match(regex)

        numberOfQa[index - 2].y = day.numberOfQa
        numberCorrect[index - 2].y = day.numberCorrect
      })

      const proccessedData = {
        numberOfQa,
        numberCorrect,
      }

      setTimeout(() => {
        resolve(proccessedData)
      }, 100)
    } catch (error) {
      reject(error)
    }
  })
}

function* doAction(action) {
  try {
    // yield put({ type: types.MONEY_HISTORY_SUCCESS, payload: { last_page, data, nextPage: passData.page + 1 } })
    const results = yield call(proccessData, MockupData.results)
    yield put({
      type: AppTypes.GET_WEEKLY_SUCCESS,
      payload: {
        ...results,
      },
    })
  } catch (error) {
    // yield put(apiActions.storeResponseMessage({ type: apiTypes.SERVER_ERROR, code: -1, isDefault: true }))
    console.log('error failed', error)
    yield put({ type: AppTypes.GET_WEEKLY_FAIL })
  }
}

export default function* watchAction() {
  // yield takeLatest(types.MONEY_HISTORY, doAction)
  yield takeLatest(AppTypes.GET_WEEKLY, doAction)
}

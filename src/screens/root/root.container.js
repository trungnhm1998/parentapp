import React, { Component } from 'react'
import { View, Text } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Navigation } from 'react-native-navigation'
import { SideMenuButton } from '../../components'
import { ScreenConfigs, AppNavigation } from '../../navigation';

export class RootContainer extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    AppNavigation.navToScreenWithSideMenu(ScreenConfigs.HOME, 'home')
  }

  render() {
    return (
      <View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {}
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RootContainer)

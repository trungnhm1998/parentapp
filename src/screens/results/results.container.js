import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { connect } from 'react-redux'
import { Navigation } from 'react-native-navigation'
import { SideMenuButton } from '../../components'

class ResultsContainer extends Component {
  render() {
    return (
      <View>
        <SideMenuButton parentId={this.props.componentId} />
        <Text> Results </Text>
      </View>
    )
  }
}

export default connect(
  null,
  null,
)(ResultsContainer)

import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { connect } from 'react-redux'
import { Navigation } from 'react-native-navigation'
import { bindActionCreators } from 'redux'
import { SideMenuButton } from '../../components'
import { AppActions } from '../../redux/app'

class ChildrenContainer extends Component {
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'green' }}>
        <SideMenuButton parentId={this.props.componentId} />
        <Text> Children </Text>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    appAction: bindActionCreators(AppActions, dispatch),
  }
}

export default connect(
  null,
  mapDispatchToProps,
)(ChildrenContainer)

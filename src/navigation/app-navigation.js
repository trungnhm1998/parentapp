import { Navigation } from 'react-native-navigation'
import ScreenConfigs from './screen.config'

const AppNavigation = {
  navToRoot: () => {
    Navigation.setRoot({
      root: {
        stack: {
          children: [
            {
              component: {
                name: ScreenConfigs.ROOT,
              },
            },
          ],
        },
      },
    })
  },
  navToScreenWithSideMenu: (componentName, activeComponent) => {
    Navigation.setRoot({
      root: {
        sideMenu: {
          left: {
            stack: {
              children: [
                {
                  component: {
                    name: ScreenConfigs.LEFT_SIDE_MENU,
                    passProps: {
                      activeComponent,
                    },
                  },
                },
              ],
            },
            // component: {
            //   name: ScreenConfigs.LEFT_SIDE_MENU,
            //   passProps: {
            //     activeComponent,
            //   },
            // },
          },
          center: {
            stack: {
              children: [
                {
                  component: {
                    name: componentName,
                  },
                },
              ],
            },
          },
        },
      },
    })
  },
}

export default AppNavigation

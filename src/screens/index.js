import Root from './root'
import Home from './home'
import Children from './children'
import Results from './results'

const AppScreens = {
  Root,
  Home,
  Children,
  Results,
}

export default AppScreens

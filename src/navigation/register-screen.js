import React from 'react'
import { Navigation } from 'react-native-navigation'
import * as Components from '../components'
import AppScreens from '../screens' 
import ScreenConfigs from './screen.config'
import StoreProvider from '../configs/store-provider.config'

type Props = {}

const WrappedComponent = (Component) => {
  return (props: Props) => {
    const HOC = () => (
      <StoreProvider>
        <Component {...props} />
      </StoreProvider>
    )

    return <HOC />
  }
}

export default () => {
  Navigation.registerComponent(ScreenConfigs.ROOT, () => WrappedComponent(AppScreens.Root))
  Navigation.registerComponent(ScreenConfigs.HOME, () => WrappedComponent(AppScreens.Home))
  Navigation.registerComponent(ScreenConfigs.CHILDREN, () => WrappedComponent(AppScreens.Children))
  Navigation.registerComponent(ScreenConfigs.RESULTS, () => WrappedComponent(AppScreens.Results))

  Navigation.registerComponent(ScreenConfigs.LEFT_SIDE_MENU, () => WrappedComponent(Components.LeftSideMenu))
  Navigation.registerComponent(ScreenConfigs.LEFT_SIDE_MENU_RESULTS, () => WrappedComponent(Components.LeftSideMenuResults))

  console.info('All screens have been registered...')
} 

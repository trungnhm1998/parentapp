import AppTypes from './app.types'

const initState = {
  // showSideMenu: false,
  weeklyResults: {
    numberOfQa: [
      {
        x: 'mon',
        y: 0,
      },
      {
        x: 'tue',
        y: 0,
      },
      {
        x: 'wed',
        y: 0,
      },
      {
        x: 'thu',
        y: 0,
      },
      {
        x: 'fri',
        y: 0,
      },
      {
        x: 'sat',
        y: 0,
      },
      {
        x: 'sun',
        y: 0,
      },
    ],
    numberCorrect: [
      
    ],
  },
}

export default function appReducer(state = initState, action) {
  switch (action.type) {
  // case AppTypes.TOGGLE_SIDE_MENU:
  //   return {
  //     ...state,
  //     showSideMenu: !state.showSideMenu,
  //   }
  case AppTypes.GET_WEEKLY_SUCCESS: {
    const { numberOfQa, numberCorrect } = action.payload
    return {
      ...state,
      weeklyResults: {
        numberOfQa,
        numberCorrect,
      },
    }
  }
  case AppTypes.GET_WEEKLY_FAIL: {
    return {
      ...state,
      weeklyResults: {
        numberOfQa: [],
        numberCorrect: [],
      },
    }
  }
  default:
    return state
  }
}

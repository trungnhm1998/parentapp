import AppTypes from './app.types'

// const toggleSideMenu = () => {
//   return {
//     type: AppTypes.TOGGLE_SIDE_MENU,
//   }
// }

const getWeeklyResults = () => {
  return {
    type: AppTypes.GET_WEEKLY,
  }
}

const AppActions = {
  // toggleSideMenu,
  getWeeklyResults,
}

export default AppActions

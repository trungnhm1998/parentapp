import { combineReducers } from 'redux'
import {
  // mockupReducer,
  appReducer,
} from '../redux'


const rootReducer = combineReducers({
  // mockupReducer,
  appReducer,
})

export default rootReducer

import React, { Component } from 'react'
import {
  View,
  ImageBackground,
  Image,
  Text,
} from 'react-native'
import { connect } from 'react-redux'
import { Navigation } from 'react-native-navigation'
import TimerMixin from 'react-timer-mixin'
import { Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/SimpleLineIcons'
import styles from './styles'
import { AppNavigation, ScreenConfigs } from '../../navigation'
import AppImages from '../../assets/images'
import { Themes } from '../../ui'

class LeftSideMenuResults extends Component {
  toggleSideMenu(toggleValue) {
    Navigation.mergeOptions(this.props.componentId, {
      sideMenu: {
        left: {
          visible: toggleValue,
        },
      },
    })
  }

  renderSideMenuNav(text, component, iconName, onPress) {
    const { activeComponent } = this.props
    const icon = (
      <Icon
        style={{ marginRight: 20, marginLeft: 10 }}
        name={iconName}
        size={20}
        color={component === activeComponent ? 'white' : 'black'}
      />
    )
    return (
      <Button
        onPress={() => onPress()}
        title={text}
        titleStyle={component === activeComponent ? styles.navTextActive : styles.navText}
        type="clear"
        icon={icon}
        buttonStyle={{
          justifyContent: 'flex-start',
        }}
      />
    )
  }

  navToHome() {
    this.toggleSideMenu(false)
    TimerMixin.setTimeout(() => {
      AppNavigation.navToScreenWithSideMenu(ScreenConfigs.HOME, 'home')
    }, 500)
  }

  navToChildren() {
    this.toggleSideMenu(false)
    TimerMixin.setTimeout(() => {
      AppNavigation.navToScreenWithSideMenu(ScreenConfigs.CHILDREN, 'children')
    }, 500)
  }

  navToResults() {
    this.toggleSideMenu(false)
    TimerMixin.setTimeout(() => {
      AppNavigation.navToScreenWithSideMenu(ScreenConfigs.RESULTS, 'results')
    }, 500)
  }

  renderNavs() {
    return (
      <View>
        {this.renderSideMenuNav('Home', 'home', 'home', this.navToHome.bind(this))}
        {this.renderSideMenuNav('Children', 'children', 'user', this.navToChildren.bind(this))}
        {this.renderSideMenuNav('Results', 'results', 'pie-chart', this.navToResults.bind(this))}
      </View>
    )
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <ImageBackground source={AppImages.sideMenuBg} style={styles.backgroundImage}>
          <View style={{ marginTop: 100 * Themes.Metrics.ratioScreen }}>
            {this.renderNavs()}
          </View>
        </ImageBackground>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    // showSideMenu: state.appReducer.showSideMenu,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // appAction: bindActionCreators(AppActions, dispatch),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LeftSideMenuResults)

import React from 'react'
import { TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import styles from './styles'
import { Themes } from '../../ui'
import { Navigation } from 'react-native-navigation';

const showSideMenu = (componentId) => {
  Navigation.mergeOptions(componentId, {
    sideMenu: {
      left: {
        visible: true,
      },
    },
  })
}

const SideMenuButton = ({ onPress, parentId }) => {
  return (
    <TouchableOpacity
      onPress={() => showSideMenu(parentId)}
      style={styles.buttonContainer}
    >
      <Icon
        name="more-vert"
        size={30 * Themes.Metrics.ratioScreen}
        color="white"
        style={{
          width: 30 * Themes.Metrics.ratioScreen,
        }}
      />
    </TouchableOpacity>
  )
}

export default SideMenuButton

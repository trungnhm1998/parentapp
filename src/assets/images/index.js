const AppImages = {
  logo: require('./starmathsonline-logo.png'),
  sideMenuBg: require('./side-menu-bg.jpg'),
  userAvatar: require('./parent-avatar.jpg'),
}

export default AppImages

import { StyleSheet } from 'react-native'
import { Themes } from '../../ui'

const { ratioScreen } = Themes.Metrics

const styles = StyleSheet.create({
  buttonContainer: {
    width: 20 * ratioScreen,
    height: 50 * ratioScreen,
    backgroundColor: '#FF6600',
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 30 * ratioScreen,
    left: 0,
  },
})

export default styles

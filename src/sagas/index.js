import { fork, all } from 'redux-saga/effects'
// import MockupSagas from './mockup';
import Result from './result'

function* rootSagas() {
  yield all([
    // fork(MockupSagas.Mockup),

    fork(Result.GetWeekly),
  ])
}

export default rootSagas

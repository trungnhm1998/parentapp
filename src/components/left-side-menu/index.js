import React, { Component } from 'react'
import {
  View,
  ImageBackground,
  Image,
  Text,
  Platform,
} from 'react-native'
import { connect } from 'react-redux'
import { Navigation } from 'react-native-navigation'
import { bindActionCreators } from 'redux'
import TimerMixin from 'react-timer-mixin'
import { Button, Header, Avatar } from 'react-native-elements'
import Icon from 'react-native-vector-icons/SimpleLineIcons'
import styles from './styles'
import { AppNavigation, ScreenConfigs } from '../../navigation'
import { AppActions } from '../../redux/app'
import AppImages from '../../assets/images'
import { Themes } from '../../ui'

class LeftSideMenu extends Component {
  toggleSideMenu(toggleValue) {
    Navigation.mergeOptions(this.props.componentId, {
      sideMenu: {
        left: {
          visible: toggleValue,
        },
      },
    })
  }

  renderSideMenuNav(text, component, iconName, onPress) {
    const { activeComponent } = this.props
    const icon = (
      <Icon
        style={{ marginRight: 20, marginLeft: 10 }}
        name={iconName}
        size={20}
        color={component === activeComponent ? 'white' : 'black'}
      />
    )
    return (
      <Button
        onPress={() => onPress()}
        title={text}
        titleStyle={component === activeComponent ? styles.navTextActive : styles.navText}
        type="clear"
        icon={icon}
        buttonStyle={{
          justifyContent: 'flex-start',
        }}
      />
    )
  }

  navToHome() {
    this.toggleSideMenu(false)
    TimerMixin.setTimeout(() => {
      AppNavigation.navToScreenWithSideMenu(ScreenConfigs.HOME, 'home')
    }, 500)
  }

  navToChildren() {
    this.toggleSideMenu(false)
    TimerMixin.setTimeout(() => {
      AppNavigation.navToScreenWithSideMenu(ScreenConfigs.CHILDREN, 'children')
    }, 500)
  }

  navToResults() {
    // this.toggleSideMenu(false)
    // TimerMixin.setTimeout(() => {
    //   AppNavigation.navToScreenWithSideMenu(ScreenConfigs.RESULTS, 'results')
    // }, 500)
    Navigation.push(this.props.componentId, {
      component: {
        name: ScreenConfigs.LEFT_SIDE_MENU_RESULTS,
        options: {
          topBar: {
            ...Platform.select({
              ios: {
                noBorder: true,
              },
              android: {
                borderColor: '#ffffff00',
                borderHeight: 0,
                elevation: 0,
              },
            }),
            visible: true,
            drawBehind: true,
            animate: false,
            background: {
              color: '#ffffff00',
            },
          },
          animations: {
            push: {
              enable: false,
            },
          },
        },
      },
    })
  }

  renderHeader() {
    const leftButton = (
      <Icon
        name="menu"
        color="#fff"
        size={20}
        onPress={() => this.toggleSideMenu(false)}
      />
    )

    const centerLogo = (
      <Image resizeMethod="resize" source={AppImages.logo} style={styles.headerLogoImage} />
    )

    return (
      <Header
        leftComponent={leftButton}
        centerComponent={centerLogo}
        containerStyle={{
          backgroundColor: 'rgba(255, 255, 255, 0)',
          borderBottomWidth: 0,
        }}
      />
    )
  }

  renderProfileOverview() {
    return (
      <View style={styles.profileOverviewContainer}>
        <Avatar rounded source={AppImages.userAvatar} size="large" />
        <Text style={styles.profileName}>Star</Text>
      </View>
    )
  }

  renderNavs() {
    return (
      <View>
        {this.renderSideMenuNav('Home', 'home', 'home', this.navToHome.bind(this))}
        {this.renderSideMenuNav('Children', 'children', 'user', this.navToChildren.bind(this))}
        {this.renderSideMenuNav('Results', 'results', 'pie-chart', this.navToResults.bind(this))}
      </View>
    )
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <ImageBackground source={AppImages.sideMenuBg} style={styles.backgroundImage}>
          {this.renderHeader()}
          <View style={{ marginTop: 100 * Themes.Metrics.ratioScreen }}>
            {this.renderProfileOverview()}
            {this.renderNavs()}
          </View>
        </ImageBackground>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    // showSideMenu: state.appReducer.showSideMenu,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // appAction: bindActionCreators(AppActions, dispatch),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LeftSideMenu)

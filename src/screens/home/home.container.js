import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { connect } from 'react-redux'
import { Navigation } from 'react-native-navigation'
import { bindActionCreators } from 'redux'
import {
  VictoryBar, VictoryChart, VictoryTheme, VictoryGroup, 
} from 'victory-native'
import { SideMenuButton } from '../../components'
import { AppActions } from '../../redux/app'

export class HomeContainer extends Component {
  // componentWillMount() {
  //   this.props.appAction.getWeeklyResults()
  // }

  constructor(props) {
    super(props)
    this.state = {
      data: this.getData(),
    }
  }

  componentDidMount() {
    this.setStateInterval = setInterval(() => {
      console.log('INTERVAL')
      this.setState({
        data: this.getData(),
      })
    }, 3000)
  }

  componentWillUnmount() {
    clearInterval(this.setStateInterval)
  }

  range(start, stop, step) {
    if (typeof stop === 'undefined') {
      // one param defined
      stop = start
      start = 0
    }

    if (typeof step === 'undefined') {
      step = 1
    }

    if ((step > 0 && start >= stop) || (step < 0 && start <= stop)) {
      return []
    }

    const result = []
    for (let i = start; step > 0 ? i < stop : i > stop; i += step) {
      result.push(i)
    }

    return result
  }

  getData() {
    return this.range(5)
      .map((bar) => {
        return { x: bar + 1, y: Math.random(0, 10) }
      })
  }

  render() {
    const { appReducer } = this.props.payload
    console.log(appReducer.weeklyResults.numberOfQa)
    const test = appReducer.weeklyResults.numberOfQa
    return (
      <View>
        <Text> Home </Text>
        <VictoryChart domainPadding={{ x: 20 }} animate={{ duration: 500 }}>
          <VictoryGroup offset={20}>
            <VictoryBar
              data={this.state.data}
              style={{
                data: { fill: 'tomato', width: 12 },
              }}
              animate={{
                onExit: {
                  duration: 500,
                  before: () => ({
                    _y: 0,
                    fill: 'orange',
                    label: 'BYE',
                  }),
                },
              }}
            />
            <VictoryBar
              data={this.state.data}
              style={{
                data: { fill: 'tomato', width: 12 },
              }}
              animate={{
                onExit: {
                  duration: 500,
                  before: () => ({
                    _y: 0,
                    fill: 'orange',
                    label: 'BYE',
                  }),
                },
              }}
            />
          </VictoryGroup>
        </VictoryChart>
        <SideMenuButton parentId={this.props.componentId} />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    payload: state,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    appAction: bindActionCreators(AppActions, dispatch),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeContainer)
